/*create database challengedb;

use challengedb;
create user 'admin'@'localhost' identified by 'admin'

grant all on challengedb.* to 'admin'@'localhost';*/

use challengedb;

drop table if exists Clients, Transactions;

create table Clients(
	clientid bigint primary key auto_increment,
    `Client Name` varchar(100),
    `Contact No.` varchar(100),
	`Mailing Address`  varchar(100),
    `Member Since` varchar(100),
    `Branch of Registration` varchar(100));
   
   
create table Transactions(
	transactionid bigint primary key auto_increment,
    `Client Name` varchar(100),
     clientid bigint,
	`Payment Mode` varchar(100),
    `Item Name` varchar(100),
    `Net Amount` float,
    `VAT` float,
    `Branch Location` varchar(100),
    Timestamp varchar(100));