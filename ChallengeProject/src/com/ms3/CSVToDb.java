package com.ms3;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CSVToDb {
	
	private static final String CSV_FILE_PATH_CLIENTS = "./CodingChallenge - Clients.csv";
	private static final String CSV_FILE_PATH_TRANSACTIONS = "./CodingChallenge - Transactions.csv";
	private String ReadResult="";
	public String BadDataList="";
	public String LogFile="";
	
	public String CSVToDbFileClients(){
		
		DbDataQuery Indb = new DbDataQuery();
		Integer count = 0;
		Integer countCols = 0;
		Integer goodrecords = 0;
		Integer badrecords = 0;
		String BadData="";
		
		try (	        		
	            Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH_CLIENTS));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
	                    .withHeader("Client Name", "Contact No.", "Mailing Address", "Member Since","Branch of Registration")
	                    .withSkipHeaderRecord()
						);
				
				) {
	            for (CSVRecord csvRecord : csvParser) {
	                // Accessing Values by Column Index
	                
	                String ClientName = csvRecord.get("Client Name");
	                String ContactNo = csvRecord.get("Contact No.");
	                String MailingAddress = csvRecord.get("Mailing Address");
	                String MemberSince = csvRecord.get("Member Since");
	                String BranchofRegistration = csvRecord.get("Branch of Registration");
	                String SQLQueryData = "INSERT INTO Clients(`Client Name`,`Contact No.`,`Mailing Address`,`Member Since`,`Branch of Registration`) VALUES('"+ClientName+"','"+ContactNo+"','"+MailingAddress+"','"+MemberSince+"','"+BranchofRegistration+"'); ";
	                
	                
	                countCols = csvRecord.size();
	                count++;
	                
	                if (countCols == 5){
	                	
	                	goodrecords++;
	                	Indb.setDbQueryInsert(SQLQueryData);
	                	Indb.InsertData();
	                }
	                
	                else{
	                	BadData = BadData + ClientName +  "," + ContactNo +  "," + MailingAddress +  "," + MemberSince +  "," + BranchofRegistration + System.lineSeparator();
	                	badrecords++;
	                }
	                	            	
	            }
	        } 		
			catch(IOException e){
				Logger lgr = Logger.getLogger("CSVToDb");
				lgr.log(Level.SEVERE, e.getMessage(), e);
			}
		
		BadDataList = BadData;
		LogFile = "Clients" + System.lineSeparator() + "Total Data Received:" + count.toString() + System.lineSeparator() +  "Total Data Success:" + goodrecords.toString() + System.lineSeparator() + "Total Data Failed:" + badrecords.toString() + System.lineSeparator();
		ReadResult = LogFile;
		return ReadResult;
	}

public String CSVToDbFileTransactions(){
		
		DbDataQuery Indb = new DbDataQuery();
		Integer count = 0;
		Integer countCols = 0;
		Integer goodrecords = 0;
		Integer badrecords = 0;
		String BadData="";
		
		try (	        		
	            Reader reader = Files.newBufferedReader(Paths.get(CSV_FILE_PATH_TRANSACTIONS));
				CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT
	                    .withHeader("Client Name","Payment Mode","Item Name","Net Amount","VAT","Branch Location","Timestamp")
	                    .withSkipHeaderRecord()
						);
				
				) {
	            for (CSVRecord csvRecord : csvParser) {
	                // Accessing Values by Column Index
	            	
	                
	                String ClientName = csvRecord.get("Client Name");
	                
	                Indb.setClientData(ClientName);
	           
					String ClientID = Indb.SearchClientData().toString();
					
	                String PaymentMode = csvRecord.get("Payment Mode");
	                String ItemName = csvRecord.get("Item Name");
	                String NetAmount = csvRecord.get("Net Amount");
	                String VAT = csvRecord.get("VAT");
					String BranchLocation = csvRecord.get("Branch Location");
					String Timestamp = csvRecord.get("Timestamp");
	                String SQLQueryData = "INSERT INTO Transactions(`Client Name`,`ClientID`,`Payment Mode`,`Item Name`,`Net Amount`,`VAT`,`Branch Location`,`Timestamp`) VALUES('"+ClientName+"','"+ClientID+"','"+PaymentMode+"','"+ItemName+"','"+NetAmount+"','"+VAT+"','"+BranchLocation+"','"+Timestamp+"'); ";
	                 
	                countCols = csvRecord.size();
	                count++;
	                
	                if (countCols == 7){
	                	
	                	goodrecords++;
	                	Indb.setDbQueryInsert(SQLQueryData);
	                	Indb.InsertData();
	                }
	                
	                else{
	                	BadData = BadData + ClientName +  "," + PaymentMode + "," + ItemName + "," + NetAmount + "," + VAT + System.lineSeparator();
	                	badrecords++;
	                }
	                	            	
	            }
	        } 		
			catch(IOException e){
				Logger lgr = Logger.getLogger("CSVToDb");
				lgr.log(Level.SEVERE, e.getMessage(), e);
			}
		BadDataList = BadData;
		LogFile = "Transactions" + System.lineSeparator() + "Total Data Received:" + count.toString() + System.lineSeparator() +  "Total Data Success:" + goodrecords.toString() + System.lineSeparator() + "Total Data Failed:" + badrecords.toString() + System.lineSeparator();
		ReadResult = LogFile;
		return ReadResult;
	}

	
}
