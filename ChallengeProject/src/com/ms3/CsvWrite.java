package com.ms3;


import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;


public class CsvWrite {
	
	
	
    public void CsvWriteBadDataClients(String ClientBadData) {
        try {
             
        	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        	LocalDateTime now = LocalDateTime.now();
        	String Dtnow = dtf.format(now);
        	
        	//We have to create the CSVPrinter class object
            Writer writer = Files.newBufferedWriter(Paths.get("./bad-record_clients_" + Dtnow + ".csv"));
            //CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("Client Name", "Contact No.", "Mailing Address", "Member Since","Branch of Registration"));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            
            //Writing records in the generated CSV file
            csvPrinter.printRecord(ClientBadData);
            
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void CsvWriteBadDataTransactions(String TransactionBadData) {
        try {
             
        	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        	LocalDateTime now = LocalDateTime.now();
        	String Dtnow = dtf.format(now);
        	
        	//We have to create the CSVPrinter class object
            Writer writer = Files.newBufferedWriter(Paths.get("./bad-record_transactions_" + Dtnow + ".csv"));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            
            //Writing records in the generated CSV file
            csvPrinter.printRecord(TransactionBadData);
            
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void CsvWriteLogFile(String LogData) {
        try {
             
        	DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        	LocalDateTime now = LocalDateTime.now();
        	String Dtnow = dtf.format(now);
        	
        	//We have to create the CSVPrinter class object
            Writer writer = Files.newBufferedWriter(Paths.get("./logfile_" + Dtnow + ".txt"));
            //CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("Client Name", "Contact No.", "Mailing Address", "Member Since","Branch of Registration"));
            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            
            //Writing records in the generated CSV file
            csvPrinter.printRecord(LogData);
            
            csvPrinter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}