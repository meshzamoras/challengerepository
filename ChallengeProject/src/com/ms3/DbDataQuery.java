package com.ms3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbDataQuery {
	
	private String DbQueryInsert;
	private String ClientData;
	
	public String getDbQueryInsert() {
		return DbQueryInsert;
	}

	public void setDbQueryInsert(String dbQueryInsert) {
		DbQueryInsert = dbQueryInsert;
	}
	
	public String getClientData() {
		return ClientData;
	}

	public void setClientData(String clientData) {
		ClientData = clientData;
	}

	public void InsertData(){
		
		String cs = "jdbc:mysql://localhost:3306/challengedb?useSSL=false";
        String user = "admin";
        String password = "admin";
       
        
        try (Connection con = DriverManager.getConnection(cs, user, password);
                Statement st = con.createStatement()) {

                String sql = getDbQueryInsert();
                st.executeUpdate(sql);
            }

         catch (SQLException ex) {

            Logger lgr = Logger.getLogger(DbDataQuery.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
             
	}
	public Integer SearchClientData(){
		
		String url = "jdbc:mysql://localhost:3306/challengedb?useSSL=false";
        String user = "admin";
        String password = "admin";
        String clientname = ClientData;
        String query = "Select clientid from Clients where `Client Name`='"+clientname+"'";
        Integer clientid = 0;      
       
        
        try (Connection con = DriverManager.getConnection(url, user, password);
                PreparedStatement pst = con.prepareStatement(query);
                ResultSet rs = pst.executeQuery()) {

            while (rs.next()) {

                clientid=rs.getInt(1);
            }

        }

         catch (SQLException ex) {

            Logger lgr = Logger.getLogger(DbDataQuery.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);
        }
        
        return clientid;
	}
}