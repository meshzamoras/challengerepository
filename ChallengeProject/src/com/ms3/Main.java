package com.ms3;

//import com.ms3.InsertDataToDb;
import com.ms3.CSVToDb;
import com.ms3.CsvWrite;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String readcsvresult;
		String file_name;
		
		Scanner user_input = new Scanner(System.in);
		CSVToDb rc = new CSVToDb();
		CsvWrite cw = new CsvWrite();
		
	
		System.out.println("Select data file. Clients or Transactions");
		file_name= user_input.next();
		
		
		if (file_name.equals("Clients")){
			
			System.out.println("Clients selected.");
			System.out.println(rc.CSVToDbFileClients());
			cw.CsvWriteBadDataClients(rc.BadDataList);
			
		}
		
		else if (file_name.equals("Transactions"))
		{
			System.out.println("Transactions selected.");
			System.out.println(rc.CSVToDbFileTransactions());
			cw.CsvWriteBadDataTransactions(rc.BadDataList);
		}
		
		else
		{
			System.out.println("Incorrect data file selected.");
		}
			
		
		cw.CsvWriteLogFile(rc.LogFile);
		
		
		
	}

}
