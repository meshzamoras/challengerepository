##a. Requirements of the coding challenge

	1.	In your assessment is there something wrong with the requirements? 
			I think there's nothing wrong with the requirements.
	2.	If yes, how do you propose the requirements and/or design can be improved?
			

##b. Instructions on how to compile and use from the command line 
	
	Note: I'm using Eclipse IDE to create this project
	
	1. Java source files are in ChallengeProject\src\com\ms3
	2. Compile and run Main.java file
	3. In the command line, it will ask to select data file. Clients or Transactions
	4. Enter data file. Example Clients. It will then process the csv file into the database and will give you the results.
	

##c. Description of different steps in your solution 
	
	**Installed and setup the Java, Maven, Github, BitBucket, MySQL. I followed the instructions from the references given to me.
	I used Eclipse IDE to compile and run the java files.
		
	**Class DBDataQuery. It will connect and execute the queries. Methods are InsertData and SearchClientData.
	Insertdata will add new data to the table. SearchClientData will search clientid using a customer name.
	
	**Class CSVtoDb. It will parse the data from a CSV file and will create query to be executed using class DBDataQuery. 
	Methods are CSVToDbFileClients and CSVToDbFileTransactions. 
	
	String BadData and Logfile are declared public to be used for result files.
	
	CSVToDbFileClients method will parse data from Client CSV file and data will be inserted to a prepared Query. 
	This method will return log file for the number of received, success and failed data for Clients.
	
	CSVToDbFileTransactions method will parse data from Transactions CSV file and data will be inserted to a prepared Query. 
	This method will return log file for the number of received, success and failed data for Transactions.
		
	**Class CsvWrite to create result files. Methods are CsvWriteBadDataClients,CsvWriteBadDataTransactions and CsvWriteLogFile.
	CsvWriteBadDataClients will create a new csv file for Clients.Its filename has a realtime timestamp it is created.
	Output CSV file contains all the bad data rejected after the Clients file is process.
	CsvWriteBadDataTransactions will create a new csv file for Transactions.Its filename has a realtime timestamp of when it is created.
	Output CSV file all the bad data rejected after the Transactions file is process.
	CsvWriteLogFile will create a new txt file for the results. Its filename has a realtime timestamp of when it is created. 
	
	**Class Main will require the user to select between Clients or Transactions. User must enter either of the two.
	If the user entered "Clients", it will read the Clients CSV file, insert the data to database table Client, 
	create result and failed data files.
	If the user entered "Transactions", it will read the Transactions CSV file, insert the data to database table Transactions, 
	create result and failed data files.
	If the user entered neither of the 2 selection, it will prompt "Incorrect data file selected".
	
	

##d. DDL commands previously mentioned 

create database challengedb;

use challengedb;
create user 'admin'@'localhost' identified by 'admin'

grant all on challengedb.* to 'admin'@'localhost';

use challengedb;

drop table if exists Clients, Transactions;

create table Clients(
	clientid bigint primary key auto_increment,
    `Client Name` varchar(100),
    `Contact No.` varchar(100),
	`Mailing Address`  varchar(100),
    `Member Since` varchar(100),
    `Branch of Registration` varchar(100));
   
   
create table Transactions(
	transactionid bigint primary key auto_increment,
    `Client Name` varchar(100),
     clientid bigint,
	`Payment Mode` varchar(100),
    `Item Name` varchar(100),
    `Net Amount` varchar(100),
    `VAT` varchar(100),
    `Branch Location` varchar(100),
    Timestamp varchar(100));
    

##e. Other References

https://docs.oracle.com/javase/tutorial/jdbc/basics/

https://docs.oracle.com/javase/7/docs/webnotes/install/windows/jdk-installation-windows.html

http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

https://docs.oracle.com/javase/8/docs/

https://dev.mysql.com/downloads/file/?id=478034

http://www.mkyong.com/maven/how-to-create-a-java-project-with-maven/

https://www.callicoder.com/java-read-write-csv-file-apache-commons-csv/

https://www.homeandlearn.co.uk/java/user_input.html

https://commons.apache.org/proper/commons-csv/apidocs/org/apache/commons/csv/CSVFormat.html

